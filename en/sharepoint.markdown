The Little SharePoint Book
==========================

## About
The Little SharePoint Book is a free ebook for SharePoint Developers.

The book was written by [Bil Simser](http://weblogs.asp.net/bsimser).

## License
The book is freely distributed under the [Attribution-NonCommercial 3.0 Unported license](<http://creativecommons.org/licenses/by-nc/3.0/legalcode>).

**You should not have paid for this book.**

You are basically free to copy, distribute, modify or display the book as long as you provide the appropriate attribution to me, Bil Simser, and do not use it for commercial purposes.

You can see the full text of the license at:

<http://creativecommons.org/licenses/by-nc/3.0/legalcode>

## Formats
The book is written in [markdown](http://daringfireball.net/projects/markdown/).

## About The Author
Bil Simser is an independent Solutions Architect with over 15 years in software development. He has helped build many large-scale mission critical systems along the way.  In his role as a mentor, he guides clients on how to implement development standards and guidelines, evaluates and recommends new tools and technologies, and helps teams and projects progress into the .NET world.  Bil also has a special interest in coaching clients on Agile and General Software Development Best Practices.

Bil has been involved with Microsoft's .NET platform since the early betas and has a deep passion for good Architecture and Software Design. He specializes in SharePoint, .NET, Agile, TDD, and computer game programming. Bil also runs several successful open-source projects.  He contributes to the software development community, taking the time to review and edit SharePoint and Agile publications, and speak at user groups, code camps, and conferences, including TechEd, DevConnections, PDC, and DevTeach.

Bil has been a Microsoft SharePoint MVP since 2004 and a member of the MSDN Canada Speakers Bureau. Bil currently lives and works in Alberta, Canada, with his wife, daughter, a Beowulf cluster of computers, every gaming console known to man, and a small menagerie of animals.

His blog can be found at <http://weblogs.asp.net/bsimser>, and he tweets via [@bsimser](http://twitter.com/bsimser)

## With Thanks To
A special thanks to [Karl Sequin](http://twitter.com/karlseguin) for inspriation and ideas from his [Little MongoDB Book Project](http://github.com/karlseguin/the-little-mongodb-book).

### Latest Version
The latest source of this book is available at: 

<http://github.com/bsimser/the-little-sharepoint-book>

Introduction
------------

Chapter 1
---------

### In This Chapter

Chapter 2
---------

### In This Chapter

Chapter 3
---------

### In This Chapter

Chapter 4
---------

### In This Chapter

Chapter 5
---------

### In This Chapter

Chapter 6
---------

### In This Chapter

Chapter 7
---------

### In This Chapter

Chapter 8
---------

### In This Chapter

Chapter 9
---------

### In This Chapter

Chapter 10
----------

### In This Chapter

Conclusion
----------